<?php
/*
    ./app/controleurs/postsControleur.php
    Contrôleur des posts
 */

namespace App\Controleurs\Users;
use \App\Modeles\User;

  function loginFormAction(\PDO $connexion){


      // Je charge la vue index dans $content1
        GLOBAL $content1,$title;
        $title = TITRE_USERS_LOGINFORM;
        ob_start();
        include '../app/vues/users/loginForm.php';
        $content1 = ob_get_clean();
  }


  function loginAction(\PDO $connexion, array $data = null){
      // je demande le user qui correspond au login/pwd
      include '../app/modeles/usersModele.php';
      $user= User\findOneByLoginPwd($connexion,$data);
      // je redirige vers le backoffice si c'est OK
      // et vers le form de connexion sinon

      if($user):
        header('location:' . ROOT_ADMIN );
    else:
      header('location:' . ROOT .  'users/login/form');
    endif;



  }
