<?php
/*
    ./app/controleurs/postsControleur.php
    Contrôleur des posts
 */

namespace App\Controleurs\Categories;
use \App\Modeles\Categorie;

  function indexAction(\PDO $connexion, array $params = []){
      // Je demande la liste des posts au modèle
        include_once '../app/modeles/categoriesModele.php';
        $menus = Categorie\findAll($connexion);

      // Je charge la vue index dans $content1

        include '../app/vues/categories/index.php';

  }


  function showAction(\PDO $connexion,int $id){
      // Je demande la categorie au modèle
        include_once '../app/modeles/categoriesModele.php';
        $categorie = Categorie\findOneById($connexion, $id);

        include_once '../app/modeles/postsModele.php';
        $posts = \App\Modeles\Post\findAll($connexion,[
          'categorie' => $id,
          'orderBy' => 'datePublication',
          'orderSens' => 'DESC'
      ]);
      
      // Je charge la vue index dans $content1
        GLOBAL $content1,$title;
        $title = $categorie['titre'];
        ob_start();
        include '../app/vues/categories/show.php';
        $content1 = ob_get_clean();
  }
