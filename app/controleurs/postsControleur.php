<?php
/*
    ./app/controleurs/postsControleur.php
    Contrôleur des posts
 */

namespace App\Controleurs\Posts;
use \App\Modeles\Post;

  function indexAction(\PDO $connexion, array $params = []){
      // Je demande la liste des posts au modèle
        include_once '../app/modeles/postsModele.php';
        $posts = Post\findAll($connexion, $params);

      // Je charge la vue index dans $content1
        GLOBAL $content1,$title;
        $title = TITRE_POSTS_INDEX;
        ob_start();
        include '../app/vues/posts/index.php';
        $content1 = ob_get_clean();
  }

  function showAction(\PDO $connexion,int $id){
      // Je demande le  post au modèle
        include_once '../app/modeles/postsModele.php';
        $post = Post\findOneById($connexion, $id);
      // Je charge la vue index dans $content1
        GLOBAL $content1,$title;
        $title = $post['titre'];
        ob_start();
        include '../app/vues/posts/show.php';
        $content1 = ob_get_clean();
  }


  function searchAction(\PDO $connexion, string $search){
    // je demande la lise des posts au modèle
    include_once '../app/modeles/postsModele.php';
    $posts = Post\findAllBySearch($connexion, $search);
    // je charge la vue search dans content1
    GLOBAL $content1,$title;
    $title = $search;
    ob_start();
    include '../app/vues/posts/search.php';
    $content1 = ob_get_clean();
  }
