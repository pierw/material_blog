<?php
/*
    ./app/routeur.php
    Routeur principal
    Il décide quelle action de quel contrôleur il faut lancer
 */



if(isset($_GET['users'])):
  include_once '../app/routeurs/usersRouteur.php';

elseif (isset($_GET['categories'])):
   include '../app/routeurs/categoriesRouteur.php';


   elseif(isset($_GET['posts'])):
      include '../app/routeurs/postsRouteur.php';

// ROUTE PAR DEFAUT
// PATTERN : /
// CTRL : postsControleur
// ACTION : index
else:
      include_once '../app/controleurs/postsControleur.php';
      \App\Controleurs\Posts\indexAction($connexion,[
        'orderBy' => 'datePublication',
        'orderSens' => 'DESC'
      ]);
endif;
