<?php
/*
    ./app/config/parametres.php
    Paramètres de l'application
 */

// Constantes de connexion
  define('DBHOTE', '127.0.0.1:3306');
  define('DBNAME', 'material_blog');
  define('DBUSER', 'root');
  define('DBPWD' , 'root');

// Zones dynamiques du template
  $title    = '';
  $content1 = '';


  // TEXTE

  define ('TITRE_POSTS_INDEX', "Liste des posts");
  define ('TITRE_USERS_LOGINFORM', "Connexion au backoffice");
