<?php
/*
    ./app/vues/templates/posts/index.php
    variables disponibles
    - $posts (ARRAY(id,titre,slug,datePublication,media,auteur , texte))
 */
?>
<?php foreach ($posts as $post): ?>
<h2>
    <a href="posts/<?php echo $post['idPost']; ?>/<?php echo $post['slugPost']; ?>"><?php echo $post['titrePost'];?></a>
</h2>
<p class="lead">
  by <a href="#"><?php echo $post['pseudo'] ?></a>
</p>
<p> Posted on
  <?php echo Noyau\Fonctions\datify($post['datePublication'],"D M Y"); ?>    </p>
<hr>
<img class="img-responsive z-depth-2" src="<?php $post['media'];?>" alt="">
<hr>
   <div> <?php echo $post['texte'] ?></div>
<a href="posts/<?php echo $post['idPost']; ?>/<?php echo $post['slugPost']; ?>">
  <button type="button" class="btn btn-info waves-effect waves-light">Read more</button>
</a>
<hr>
<?php endforeach; ?>
