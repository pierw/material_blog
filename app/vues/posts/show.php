<?php
/*
    ./app/vues/templates/posts/show.php
    variables disponibles
    - $posts (ARRAY(id,titre,slug,datePublication,media,auteur , texte))
 */
?>

<!-- Blog Post -->

<!-- Title -->
<h1><?php echo $post['titre'] ?></h1>

<!-- Author -->
<p class="lead">
by <a href="#"><?php $post['pseudo'] ?></a>
</p>

<hr>

<!-- Date/Time -->
<p>Posted on
<?php echo  Noyau\Fonctions\datify($post['datePublication'],"D M Y") ?></p>

<hr>

<!-- Preview Image -->
<img class="img-responsive z-depth-2" src="<?php echo $post['media'] ?>" alt="">

<hr>

<!-- Post Content -->
<div><?php echo $post['texte']; ?></div>


<hr>
