<?php
/*
    ./app/vues/templates/categories/menu.php
    variables disponibles
    - $menu (ARRAY(id,titre,slug,))
 */
?>
<h4>Categories</h4>

<ul class="collection">
<?php foreach ($menus as $menu): ?>
  <li>
    <a href="categories/<?php echo $menu['id'];?>/<?php echo $menu['slug'];?>" class="collection-item">
      <?php echo $menu['titre'] ?>    </a>
  </li>
<?php endforeach; ?>
</ul>
