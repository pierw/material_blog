<?php
/*
    ./app/modeles/xxxsModele.php
    Modèle des xxxs
 */

namespace App\Modeles\Post;

  function findAll(\PDO $connexion, array $params = []){
    $params_default = [
      'categorie' => null,
      'orderBy' => 'id',
      'orderSens' => 'ASC',
      'limit' => null
    ];
    $params = array_merge($params_default, $params);

    $orderBy   = htmlentities($params['orderBy']);
    $orderSens = htmlentities($params['orderSens']);

    $sql = "SELECT *,posts.id AS idPost,
                     posts.titre AS titrePost,
                     posts.slug AS slugPost
            FROM posts
            JOIN auteurs ON auteur = auteurs.id ";
            if ($params['categorie'] !== null):
            $sql .= " JOIN posts_has_categories ON post = posts.id
                      WHERE categorie = :categorie ";
             endif;

    $sql .= "ORDER BY $orderBy $orderSens";

    $sql .= ($params['limit'] !== null)? "LIMIT :limit ;":';';

    $rs = $connexion->prepare($sql);
    if($params['limit'] !== null):
      $rs->bindvalue(':limit', $params['limit'], \PDO::PARAM_INT);
      endif;
    if($params['categorie'] !== null):
      $rs->bindvalue(':categorie', $params['categorie'], \PDO::PARAM_INT);
      endif;
    $rs->execute();
    return $rs->fetchAll(\PDO::FETCH_ASSOC);
  }








  function findOneById(\PDO $connexion, int $id){
    $sql = " SELECT *
             FROM posts
             JOIN auteurs ON auteur = auteurs.id
             WHERE posts.id = :id;";
    $rs = $connexion->prepare($sql);
    $rs->bindValue(':id', $id , \PDO::PARAM_STR);
    $rs->execute();
    return $rs->fetch(\PDO::FETCH_ASSOC);
  }



  function findAllBySearch(\PDO $connexion , string $search){
    $words = explode(' ', trim($search));
    $sql = "SELECT DISTINCT posts.titre AS titrePost,
                            posts.slug AS slugPost,
                            posts.id AS idPost,
                            texte,media,datePublication,pseudo
            FROM posts
            JOIN auteurs ON auteur = auteurs.id
            JOIN posts_has_categories ON post = posts.id
            JOIN categories ON categorie = categories.id
            WHERE 1=0 ";
            for ($i=0;$i<count($words);$i++):
            $sql.= " OR posts.titre      LIKE :word$i
            OR texte            LIKE :word$i
            OR categories.titre LIKE :word$i
            OR pseudo           LIKE :word$i ";
            endfor;
            $sql .= ";";
            
            $rs = $connexion->prepare($sql);
            for ($i=0;$i<count($words);$i++):
            $rs->bindValue(":word$i", '%'.$words[$i].'%' , \PDO::PARAM_STR);
            endfor;
            $rs->execute();
            return $rs->fetchAll(\PDO::FETCH_ASSOC);

  }
